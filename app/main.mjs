import * as path from 'path';
import express from 'express';
import cors from 'cors';
import TwitchEmoticons from '@mkody/twitch-emoticons';
import { ApiClient } from '@twurple/api';
import { AppTokenAuthProvider } from '@twurple/auth';

import CustomEmoteFetcher from './components/emoteFetcher.mjs';

const app = express();

const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;
const authProvider = new AppTokenAuthProvider(clientId, clientSecret);
const apiClient = new ApiClient({ authProvider });
const { EmoteFetcher } = TwitchEmoticons;
const emoteFetcher = new EmoteFetcher(clientId, clientSecret);
const customEmoteFetcher = new CustomEmoteFetcher();

app.use(cors());
app.use((req, res, next)=>{
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

/**
 * @api {get} /id/:username Get user ID
 * @apiVersion 1.0.2
 * @apiGroup API
 * @apiName GetUserID
 *
 * @apiParam {String} username Username
 *
 * @apiSuccess (Success) {String} username Username
 * @apiSuccess (Success) {String} id User ID
 * @apiSuccessExample {json} HTTP 200
 *      {
 *        "username": "twitch",
 *        "id": "12826"
 *      }
 *
 *  @apiError (Error) {String} error Error description
 *  @apiErrorExample {json} HTTP 404
 *      {
 *        "error": "User not found"
 *      }
 *  @apiErrorExample {json} HTTP 400
 *      {
 *        "error": "Username is required"
 *      }
 */
app.get('/id/:username', async (req, res) => {
  try {
    if (req.params.username) {
      const user = await apiClient.users.getUserByName(req.params.username);
      if (user) {
        res.status(200).send({
          username: req.params.username,
          id: user.id
        });
      } else {
        res.status(404).send({ error: 'User not found' });
      }
    } else {
      res.status(400).send({ error: 'Username is required' });
    }
  } catch (e) {
    console.error(e);
    res.sendStatus(500);
  }
});

/**
 * @api {get} /status/:username Get stream status
 * @apiVersion 1.0.2
 * @apiGroup API
 * @apiName GetUserStatus
 *
 * @apiParam {String} username Username
 *
 * @apiSuccess (Success) {String} username Username
 * @apiSuccess (Success) {String} status Stream status
 * @apiSuccessExample {json} HTTP 200
 *      {
 *        "username": "twitch",
 *        "status": "offline"
 *      }
 *
 *  @apiError (Error) {String} error Error description
 *  @apiErrorExample {json} HTTP 400
 *      {
 *        "error": "Username is required"
 *      }
 */
app.get('/status/:username', async (req, res) => {
  try {
    if (req.params.username) {
      const stream = await apiClient.streams.getStreamByUserName(req.params.username);
      res.status(200).send({
        username: req.params.username,
        status: stream ? 'live' : 'offline'
      });
    } else {
      res.status(400).send({ error: 'Username is required' });
    }
  } catch (e) {
    console.error(e);
    res.sendStatus(500);
  }
});

/**
 * @api {get} /emotes/:username Get user emotes
 * @apiVersion 1.0.2
 * @apiGroup API
 * @apiName GetUserEmotes
 *
 * @apiParam {String} username Username
 * @apiQuery {Boolean} [globals] Include global Twitch emotes
 * @apiQuery {String} [only] Comma-separated list of emote sources (one or more of following: `twitch, bttv, ffz, 7tv`). Request without this query parameter is equivalent to `only=twitch,bttv,ffz,7tv`
 *
 * @apiSuccess (Success) {String} username Username
 * @apiSuccess (Success) {Object} emotes User emotes
 * @apiSuccess (Success) {Array} [emotes.twitch] Twitch emotes
 * @apiSuccess (Success) {Array} [emotes.bttv] BetterTTV emotes
 * @apiSuccess (Success) {Array} [emotes.ffz] FrankerFaceZ emotes
 * @apiSuccess (Success) {Array} [emotes.7tv] 7TV emotes
 * @apiSuccessExample {json} HTTP 200
 *      {
 *        "username": "twitch",
 *        "emotes": {
 *          "twitch": [
 *            {
 *              "code": "twitchPrince",
 *              "id": "12283"
 *            },
 *            {
 *              "code": "twitchAsk",
 *              "id": "emotesv2_7d7473ef8ba54ce2b2f8e29d078f90bf"
 *            },
 *            {
 *              "code": "twitchEnvy",
 *              "id": "12281"
 *            },
 *            ...
 *          ],
 *          "bttv": [],
 *          "ffz": [],
 *          "7tv": []
 *        }
 *      }
 *
 *  @apiError (Error) {String} error Error description
 *  @apiErrorExample {json} HTTP 404
 *      {
 *        "error": "User not found"
 *      }
 *  @apiErrorExample {json} HTTP 400
 *      {
 *        "error": "Username is required"
 *      }
 */
app.get('/emotes/:username', async (req, res) => {
  try {
    if (req.params.username) {
      const user = await apiClient.users.getUserByName(req.params.username);
      if (user) {
        const id = parseInt(user.id, 10);
        const includeGlobals = req.query.globals === 'true';
        const defaultSources = ['twitch', 'bttv', 'ffz', '7tv'];
        const sources = req.query.only?.split(',').filter(x => defaultSources.indexOf(x) !== -1) || defaultSources;

        // fetch and cache emotes
        await Promise.allSettled([
          // Twitch global
          (sources.includes('twitch') && includeGlobals ? emoteFetcher.fetchTwitchEmotes() : undefined),
          // Twitch channel
          (sources.includes('twitch') ? emoteFetcher.fetchTwitchEmotes(id) : undefined),
          // BTTV global
          (sources.includes('bttv') && includeGlobals ? emoteFetcher.fetchBTTVEmotes() : undefined),
          // BTTV channel
          (sources.includes('bttv') ? emoteFetcher.fetchBTTVEmotes(id) : undefined),
          // FFZ channel
          (sources.includes('ffz') ? emoteFetcher.fetchFFZEmotes(id) : undefined),
          // 7TV global
          (sources.includes('7tv') && includeGlobals ? customEmoteFetcher.fetchSevenTvEmotes() : undefined),
          // 7TV channel
          (sources.includes('7tv') ? customEmoteFetcher.fetchSevenTvEmotes(id) : undefined)
        ]);

        const twitchSubEmotes = new Map();
        (await apiClient.chat.getChannelEmotes(user.id)).forEach(emote => {
          if (emote.tier) twitchSubEmotes.set(emote.id, emote.tier);
        });

        const emotes = {};
        sources.forEach(source => emotes[source] = []);
        emoteFetcher.emotes.forEach(emote => {
          const tier = emote.type === 'twitch' && twitchSubEmotes.has(emote.id)
            ? twitchSubEmotes.get(emote.id)
            : undefined;

          emotes[emote.type].push({
            code: emote.code,
            id: emote.id,
            tier
          });
        });
        emoteFetcher.emotes.clear();
        customEmoteFetcher.emotes.forEach(emote => {
          emotes[emote.type].push({
            code: emote.code,
            id: emote.id
          });
        });
        customEmoteFetcher.clearEmotes();

        res.status(200).send({
          username: req.params.username,
          emotes
        });
      } else {
        res.status(404).send({ error: 'User not found' });
      }
    } else {
      res.status(400).send({ error: 'Username is required' });
    }
  } catch (e) {
    console.error(e);
    res.sendStatus(500);
  }
});

/**
 * @api {get} /username/:id Get user ID
 * @apiVersion 1.0.4
 * @apiGroup API
 * @apiName GetUsername
 *
 * @apiParam {String} id User ID
 *
 * @apiSuccess (Success) {String} id User ID
 * @apiSuccess (Success) {String} username Username
 * @apiSuccessExample {json} HTTP 200
 *      {
 *        "id": "12826",
 *        "username": "twitch"
 *      }
 *
 *  @apiError (Error) {String} error Error description
 *  @apiErrorExample {json} HTTP 404
 *      {
 *        "error": "User not found"
 *      }
 *  @apiErrorExample {json} HTTP 400
 *      {
 *        "error": "ID is required"
 *      }
 */
app.get('/username/:id', async (req, res) => {
  try {
    if (req.params.id) {
      const user = await apiClient.users.getUserById(req.params.id);
      if (user) {
        res.status(200).send({
          id: user.id,
          username: user.name
        });
      } else {
        res.status(404).send({ error: 'User not found' });
      }
    } else {
      res.status(400).send({ error: 'ID is required' });
    }
  } catch (e) {
    console.error(e);
    res.sendStatus(500);
  }
});

app.use('/docs', express.static(path.join(path.resolve(), '.docs')));

app.get('*', (req, res) => {
  res.status(404).send();
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
