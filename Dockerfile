FROM node:20.12.1-alpine as build-image

WORKDIR /opt/kadesign/helix-proxy

COPY . .
RUN yarn && yarn generate-docs
RUN rm -rf node_modules
RUN yarn --prod --ignore-optional

###

FROM node:20.12.1-alpine as production-image

ENV PORT=3000

WORKDIR /opt/kadesign/helix-proxy

COPY --from=build-image /opt/kadesign/helix-proxy ./

ENTRYPOINT [ "yarn", "start" ]
EXPOSE $PORT
